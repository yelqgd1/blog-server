const jwt = require('jsonwebtoken')
const { User } = require('../models')

const SECRET = 'mocksdfnkasdffffwier'

const auth = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization
    if (!authHeader) {
      return res.send({
        code: 401,
        msg: '请登录'
      })
    }
    const raw = authHeader.split(' ').pop()
    const { id } = await jwt.verify(raw, SECRET)

    req.user = await User.findById(id)
    next()
  } catch (error) {
    res.send({
      code: 401,
      msg: '请登录'
    })
  }
}

module.exports = { auth }
