const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
// const { auth } = require('./middlewares')

mongoose.connect('mongodb://localhost/test', {
  useUnifiedTopology: true,
  useNewUrlParser: true
})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('connected', function() {
  console.log('we are connected!')
})
db.on('disconnected', () => {
  console.log('disconnected  ')
})

const articleRouter = require('./router/article')
const userRouter = require('./router/user')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/api', userRouter)
app.use('/api', articleRouter)

app.listen(3000, function() {
  console.log('Example app listening on port 3000!')
})
