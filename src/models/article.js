const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  title: String,
  description: String,
  content: String,
  created_at: {
    type: Number,
    default: Date.now
  },
  published_at: {
    type: Number,
    default: null
  },
  updated_at: {
    type: Number,
    default: null
  }
})
const Article = mongoose.model('Article', schema)

module.exports = Article
