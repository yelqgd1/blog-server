const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const userSchema = new mongoose.Schema({
  username: String,
  password: {
    type: String,
    set(value) {
      return bcrypt.hashSync(value, 10)
    }
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: null
  }
})
const User = mongoose.model('User', userSchema)

module.exports = User