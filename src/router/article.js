const express = require('express')
const router = express.Router()
const { Article } = require('../models')

router.get('/article', async (req, res) => {
  let { size = 10, page = 1, published = 0 } = req.query
  size = Number(size)
  page = Number(page)
  published = Number(published)
  const publishedOption = published ? { published_at: { $ne: null } } : {}
  const total = await Article.countDocuments({ ...publishedOption })
  const articles = await Article.find({ ...publishedOption }, '-__v -content')
    .skip((page - 1) * size)
    .limit(size)
    .sort({
      created_at: -1
    })

  res.send({
    code: 200,
    msg: 'ok',
    data: {
      total,
      list: articles
    }
  })
})

router.get('/article/:id', async (req, res) => {
  const { id } = req.params
  const article = await Article.findById(id)

  res.send({
    code: 200,
    msg: 'ok',
    data: article
  })
})

router.post('/article/add', async (req, res) => {
  const { title, description, content } = req.body
  const data = await Article.create({
    title,
    description: description || content.slice(0, 160),
    content
  })

  res.send({
    code: 200,
    msg: 'ok',
    data
  })
})

router.post('/article/delete', async (req, res) => {
  const { id } = req.body
  const data = await Article.deleteOne({ _id: id })

  res.send({
    code: 200,
    msg: 'ok',
    data
  })
})

router.post('/article/update', async (req, res) => {
  const { title, description, content, id } = req.body
  await Article.updateOne(
    { _id: id },
    {
      title,
      description: description || content.slice(0, 160),
      content,
      updated_at: Date.now()
    }
  )

  res.send({
    code: 200,
    msg: 'ok'
  })
})

router.post('/article/publish', async (req, res) => {
  const { id } = req.body
  const now = Date.now()
  await Article.updateOne(
    { _id: id },
    {
      published_at: now,
      updated_at: now
    }
  )

  res.send({
    code: 200,
    msg: 'ok'
  })
})

router.post('/article/unpublish', async (req, res) => {
  const { id } = req.body
  const now = Date.now()
  await Article.updateOne(
    { _id: id },
    {
      published_at: null,
      updated_at: now
    }
  )

  res.send({
    code: 200,
    msg: 'ok'
  })
})

module.exports = router
