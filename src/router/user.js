const express = require('express')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const router = express.Router()
const { User } = require('../models')
const { auth } = require('../middlewares')

const SECRET = 'mocksdfnkasdffffwier'

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

router.get('/profile', auth, async (req, res) => {
  res.send(req.user)
})

router.post('/login', async (req, res) => {
  const { username, password } = req.body
  const user = await User.findOne({
    username
  })

  if (!user) {
    return res.send({
      code: 40012,
      msg: '用户名不存在'
    })
  }

  if (!bcrypt.compareSync(password, user.password)) {
    return res.send({
      code: 40013,
      msg: '密码无效'
    })
  }

  const token = jwt.sign({ id: String(user._id) }, SECRET, {
    expiresIn: '7 days'
  })

  res.send({
    code: 200,
    msg: 'ok',
    data: {
      username,
      token
    }
  })
})

router.post('/user', async (req, res) => {
  const { username, password } = req.body
  const data = await User.create({
    username,
    password
  })

  res.send({
    code: 200,
    msg: 'ok',
    data
  })
})

module.exports = router
